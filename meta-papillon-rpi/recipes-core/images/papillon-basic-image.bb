# Base this image on agl-image-ivi.bb
require recipes-ivi/images/agl-image-ivi.bb

inherit extrausers
EXTRA_USERS_PARAMS = "usermod -P root root;"


# codec and service support play audio
IMAGE_INSTALL += " \
    pulseaudio \
    gstreamer1.0-plugins-ugly \
"

# Psplash causes crash on first boot on RPi
IMAGE_FEATURES_remove = "splash"

# wireless network support
IMAGE_INSTALL += " \
    iw \
    wireless-tools \
    linux-firmware-brcm43430 \
"
